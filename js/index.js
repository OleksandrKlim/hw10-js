"use strict";
let menuLister = document.querySelector(".tabs");
let list = document.querySelectorAll(".tabs > li");
let id = document.querySelectorAll(".tabs-content>li");

menuLister.addEventListener("click", function (event) {
  if (event.target.closest("li")) {
    event.target.classList.add("active");
    for (const item of list) {
      if (item !== event.target.closest("li")) {
        item.classList.remove("active");
      }
    }
  }
  get();
});

function get(params) {
  let ident;
  for (const li of list) {
    if (li.classList.contains("active")) {
      ident = li.textContent;
    }
    for (const li of id) {
      if (li.getAttribute("id") === ident) {
        li.style.display = "block";
      } else {
        li.style.display = "none";
      }
    }
  }
}
get();
